import java.io.*;

class Demo {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Enter a number:");
        int number = Integer.parseInt(br.readLine());

        System.out.println("You entered: " + number);
    }
}

