class ThreadExample {
    public static void main(String[] args) {
        // Anonymous inner class extending Thread
        Thread t = new Thread() {
            public void run() {
                System.out.println("Running in the anonymous inner class!");
            }
        };
        t.start();  // Output: Running in the anonymous inner class!
    }
}

