class Demo{
}
class DemoChild extends Demo{
}
class Parent{
	public Object fun(){
		return new Object();
	}
	Object run(){
		return new Object();
	}
}
class Child extends Parent{
	public String fun(){
		return new String("monu");
	}
	public Demo run(){
		return new Demo();
	}
	public static void main(String[] a){
		Parent p=new Child();
		System.out.println(p.fun());
		System.out.println(p.run());
	}
}

