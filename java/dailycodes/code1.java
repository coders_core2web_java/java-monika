import java.util.*;
import java.util.regex.*;
 class Solution {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        try {
            if (scanner.hasNextLine()) {
                int testCases = Integer.parseInt(scanner.nextLine().trim());

                while (testCases-- > 0) {
                    if (scanner.hasNextLine()) {
                        String pattern = scanner.nextLine();
                        try {
                            // Try to compile the regex pattern
                            Pattern.compile(pattern);
                            System.out.println("Valid");
                        } catch (PatternSyntaxException e) {
                            // Catch invalid regex patterns
                            System.out.println("Invalid");
                        }
                    } else {
                        System.out.println("Invalid");
                    }
                }
            }
        } finally {
            scanner.close();
        }
    }
}

