import java.util.*;
class Hospital implements Comparable<Hospital>{
	int Pat_id;
	String Name;
	String Diesese;
	float Bill;
	Hospital(int Pat_id,String Name,String Diesese,float Bill){
		this.Pat_id=Pat_id;
		this.Name=Name;
		this.Diesese=Diesese;
		this.Bill=Bill;
	}
	public int compareTo(Hospital obj){
		return (int)(Bill - obj.Bill);
	}
	public String toString(){
		return Name;
	}
}
class Demo{
	public static void main(String[] a){
		TreeSet t= new TreeSet();
		t.add(new Hospital(12,"monika","cold",52076666678.2f));
		t.add(new Hospital(23,"huyi","Dengue",5698f));
		t.add(new Hospital(89,"noiuk","Fever",89625.5f));
		System.out.println(t);
	}
}

