import java.util.*;
class Demo implements Comparable<Demo>{
	String str=null;
	Demo(String str){
		this.str=str;
	}
	public int compareTo(Demo obj){
		return -(str.compareTo(obj.str));
	}
	public String toString(){
		return str;
	}
}
class My{
        public static void main(String[] ar){
	TreeSet t=new TreeSet();
	t.add(new Demo("kanaha"));
	t.add(new Demo("mona"));
	t.add(new Demo("jijai"));
	System.out.println(t);
	}
}
