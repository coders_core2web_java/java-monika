import java.util.concurrent.*;
class Producer implements Runnable{
	BlockingQueue bq=null;
	Producer(BlockingQueue bq){
		this.bq=bq;
	}
	public void run(){
		for(int i=1;i<=10;i++){
			try{
				bq.put(i);
				System.out.println("producer"+i);
			}catch(InterruptedException ie){
			
			}
			try{
				Thread.sleep(1000);

			}catch(InterruptedException ie){
			
			}
		}
	}
}
class Consumer implements Runnable{
	BlockingQueue bq=null;
	Consumer(BlockingQueue bq){
		this.bq=bq;
	}
	public void run(){
		for(int i=1;i<=10;i++){
			try{
				bq.take();
				System.out.println("consumer"+i);
			}catch(InterruptedException ie){
			}
		}
	}
}
class ProducerConsumerDemo{
	public static void main(String[] a){
		BlockingQueue bq=new ArrayBlockingQueue(10);
		Producer producer=new Producer(bq);
		Consumer consumer=new Consumer(bq);

		Thread pThread=new Thread(producer);
		Thread cThread=new Thread(consumer);
		pThread.start();
		cThread.start();
	}
}
