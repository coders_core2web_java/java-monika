import java.util.*;
class Demo{
	public static void main(String[] a){
		Map m=new HashMap();
		m.put("D","Amazom");
		m.put("A","Microsoft");
		m.put("C","Apple");
		m.put("B","Google");	
		m.put("E","Google");
		
		System.out.println(m);
		
		System.out.println(m.remove("C"));
		System.out.println(m);

		System.out.println(m.containsKey("C"));

		System.out.println(m.containsValue("Amazom"));

		System.out.println(m.equals("Google"));

		m.clear();
		System.out.println(m);
	}
}
