import java.util.*;
class Player implements Comparable<Player>{
	int jerNo;
	String pName;
		Player(int jerNo,String pName){
		this.jerNo=jerNo;
		this.pName=pName;
	}
	public int compareTo(Player obj){
		return jerNo - obj.jerNo;
	}
	public String toString(){
		return "{" + jerNo + ":" + pName +"}";
	}
}
class Demo{
	public static void main(String[] a){
		TreeMap t=new TreeMap();
		Player p1=new Player(16,"MSD");
		Player p2=new Player(0,"monika");
		Player p3=new Player(16,"yash");
		Player p4=new Player(1,"paranasli");

		t.put(p1,"Ind");
		t.put(p2,"Aus");
		t.put(p3,"US");
		t.put(p4,"Pak");

		System.out.println(t);
	}
}
