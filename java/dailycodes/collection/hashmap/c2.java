import java.util.*;
class Demo{
	int data;
	Demo(int data){
		this.data=data;
	}
	public void finalize(){
		System.out.println("in finalize " + data);
	}
	public String toString(){
		return data + " ";
	}
}
class My{
	public static void main(String[] a){
		TreeMap h = new TreeMap();
		Demo obj1=new Demo(10);
		Demo obj2=new Demo(20);
		Demo obj3=new Demo(30);

		h.put(obj1,"mon");
		h.put(obj2,"Tue");
		h.put(obj3,"wed");
		
		System.out.println(h);
		
		obj1=null;
		obj2=null;
		System.gc();
		System.out.println(h);
	}
}
