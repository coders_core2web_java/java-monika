class My extends Thread{
	public void run(){
		System.out.println(getName());
		System.out.println(Thread.currentThread());
	}
}
class Monika implements Runnable{
	public void run(){
		System.out.println(Thread.currentThread().getName());
		System.out.println(Thread.currentThread());
	}
}
class Client{
	public static void main(String[] args){
		Monika m = new Monika();
		Thread t1=new Thread(m);
		t1.start();

		My mo = new My();
		mo.start();
	}
}

