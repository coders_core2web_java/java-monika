class Demo{
	Demo(){
		System.out.println("Demo constructor");
	}
	void fun(){
		System.out.println("in fun");
	}
	public void finalize(){
		System.out.println("In finalize");
	}
}
class Client{
	public static void main(String[] a)throws InterruptedException{
		System.out.println("Start main");
		Demo obj=new Demo();
		obj.fun();obj=null;
		System.gc();
		Thread.sleep(5000);
		System.out.println("end main");
	}
}
