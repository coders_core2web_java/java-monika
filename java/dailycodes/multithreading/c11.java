class Demo extends Thread{
	public void run(){
		for(int i=0;i<10;i++){
			System.out.println("In Run");
			try{
				Thread.sleep(5000);
			}
			catch(InterruptedException e){
				System.out.println(e);
			}
		}
	}
}
class Client{
	public static void main(String[] a)throws InterruptedException {
		Demo d=new Demo();
		d.start();
		d.join();	
		for(int i=0;i<=10;i++){
			System.out.println("In main");
		}
	}
}
