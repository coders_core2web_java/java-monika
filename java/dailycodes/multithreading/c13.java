import java.util.concurrent.*;  // Import correct package

class Demo implements Runnable {  // Change to implement Runnable
    int num = 0;

    Demo(int num) {
        this.num = num;
    }

    public void run() {
        System.out.println(Thread.currentThread().getName() + " start - " + num);
        fun();
        System.out.println(Thread.currentThread().getName() + " end - " + num);
    }

    void fun() {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {  // Handle exception properly
            e.printStackTrace();
        }
    }
}

class Client {
    public static void main(String[] a) {
        ExecutorService t = Executors.newFixedThreadPool(10); // Fix spelling

        for (int i = 0; i < 10; i++) {  // Use < 10 instead of <= 10
            Demo d = new Demo(i);
            t.execute(d);   // Execute the task
        }

        t.shutdown();  // Shutdown executor properly
    }
}

