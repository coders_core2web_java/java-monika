class Demo {
    public static void main(String[] args) {
        int row = 4; // Number of rows in the pattern
        
        for (int i = 1; i <= row; i++) {
            // Print leading spaces
            for (int sp = 1; sp < i; sp++) {
                System.out.print("  "); // Adjust spacing to align the pattern
            }

            // Print alternating 1s and 0s
            for (int j = 1; j <= (2 * (row - i) + 1); j++) {
                if (j % 2 == 1) {
                    System.out.print("1 ");
                } else {
                    System.out.print("0 ");
                }
            }
            
            // Move to the next line
            System.out.println();
        }
    }
}

