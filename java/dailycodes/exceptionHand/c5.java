import java.io.*;
class Demo{
	public static void main(String[] a){
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		int x=0;
		int y=0;
		try{
			x=Integer.parseInt(br.readLine());
			y=Integer.parseInt(br.readLine());
			Thread.sleep(3000);
			System.out.println(x/y);
		}
		catch(IOException ie){
			System.out.println("IOEXCEPTION");
		}
		catch(NumberFormatException ne){
			System.out.println("NUMBERFORMATE");
		}
		catch(ArithmeticException ae){
			System.out.println("Arithmatic");
		}
		catch(InterruptedException ie){
			System.out.println("Interrupted");
		}
	}
}
