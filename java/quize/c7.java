class Demo{
		Demo(){
			System.out.println("In Demo");
		}
		Demo(int num){
			System.out.println(num);
		}
		void Demo(String str){
			System.out.println(str);
		}
		static{
			Demo obj=new Demo();
			Demo obj1=new Demo(124);
			obj.Demo("coders");
			obj1.Demo("code");
		}
		public static void main(String[] a){
			System.out.println("IN MAIN METHOD");
	}
}
