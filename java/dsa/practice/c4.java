class Demo{
	static int sqrt(int num){
		int start=0;
		int end=num;
		int ans=0;
		while(start<=end){
			int mid=(start+end)/2;
			int sqr=mid*mid;
			if(sqr==num)
				return mid;
			if(sqr>num)
				end=mid-1;
			else
				start=mid+1;
		}
		return ans;
	}
	public static void main(String[] a){
		int num=81;
		double ret=sqrt(num);
		System.out.println(ret);
	
	}}
