import java.util.*;

class Demo {
    public static void main(String[] a) {
        Scanner sc = new Scanner(System.in);
        String st = sc.nextLine();
        
        // Use .equals() to compare string values
        if (st.equals("yes")) {
            System.out.println("Yesss");
        } else if (st.equals("no")) {
            System.out.println("no");
        } else {
            System.out.println("Invalid");
        }
    }
}

